const express = require("express");
const ejs = require("ejs");
const app = express();
const fs = require("fs");
const path = require("path"); // Add this line to import the path module
const { sequelize, user_game, user_game_biodata, user_game_history } = require("./models");
const methodOverride = require('method-override');
const User = require('./models');

app.use(express.static("public"));
app.use(express.urlencoded({ extended: false }));
app.set("view engine", "ejs");
app.set('views', path.join(__dirname, 'views'));
app.use(express.json());
app.use(methodOverride('_method'));

app.get("/", function (req, res) {
  res.render("pages/index");
});

app.get("/users/new", function (req, res) {
  res.render('pages/create');
});

app.get("/suit", function (req, res) {
  res.render("pages/index-suit");
});

app.get("/welcome", function (req, res) {
  res.render("pages/welcome");
});

app.get("/login", (req, res) => {
  const userData = JSON.parse(fs.readFileSync("users.json", "utf8"));
  const { username, password } = req.query;

  const user = userData.users.find(
    (user) => user.username === username && user.password === password
  );

  if (user) {
    // Successful login
    res.redirect("/users");
  } else {
    // Invalid credentials
    res.render("pages/login", { error: "Invalid username or password" });
  }
});


app.get('/users', async (req, res) => {
  try {
    const users = await user_game.findAll({
      include: [
        { model: user_game_biodata, as: 'user_game_biodata' },
        { model: user_game_history, as: 'user_game_history' },
      ],
    });

    res.render('pages/dashboard', {
      users,
    });
  } catch (error) {
    console.error('Error fetching users:', error);
    res.status(500).send('Internal Server Error: ' + error.message);
  }
});

app.get('/users/:id', async (req, res) => {
  try {
    const user = await user_game.findOne({
      where: { id: req.params.id },
      include: [
        { model: user_game_biodata, as: 'user_game_biodata' },
        { model: user_game_history, as: 'user_game_history' },
      ],
    });

    if (!user) {
      return res.status(404).send('User not found');
    }

    res.render('pages/details', {
      user,
    });
  } catch (error) {
    console.error('Error fetching user details:', error);
    res.status(500).send('Internal Server Error: ' + error.message);
  }
});

app.post('/users/new', async (req, res) => {
  try {
    const { username, email, password, fullname, religion, address, previousactivity, previousgameplayed } = req.body;

    // Create a new user in the database along with associated user_game_biodata and user_game_history
    const newUser = await user_game.create(
      {
        username,
        email,
        password,
        user_game_biodata: {
          fullname,
          religion,
          address,
        },
        user_game_history: {
          previousactivity,
          previousgameplayed,
        },
      },
      {
        include: [
          { model: user_game_biodata, as: 'user_game_biodata' },
          { model: user_game_history, as: 'user_game_history' },
        ],
      }
    );

    res.redirect('/users'); // Redirect to the users listing page after successful creation
  } catch (error) {
    console.error('Error creating user:', error);
    res.status(500).send('Internal Server Error: ' + error.message);
  }
});


app.get('/users/:id/edit', async (req, res) => {
  try {
    const user = await user_game.findOne({
      where: { id: req.params.id },
      include: [
        { model: user_game_biodata, as: 'user_game_biodata' },
        { model: user_game_history, as: 'user_game_history' },
      ],
    });

    if (!user) {
      return res.status(404).send('User not found');
    }

    res.render('pages/update', { user });
  } catch (error) {
    console.error('Error fetching user details:', error);
    res.status(500).send('Internal Server Error: ' + error.message);
  }
});


const updateUserHandler = async (req, res) => {
  try {
    const { username, email, password, fullname, religion, address, previousactivity, previousgameplayed } = req.body;
    const userId = req.params.id;

    // Update user_game table
    await user_game.update(
      {
        username,
        email,
        password,
      },
      { where: { id: userId } }
    );

    // Update user_game_biodata table
    let biodata = await user_game_biodata.findOne({ where: { user_key: userId } });
    if (biodata) {
      await biodata.update({
        fullname,
        religion,
        address,
      });
    } else {
      biodata = await user_game_biodata.create({
        user_key: userId,
        fullname,
        religion,
        address,
      });
    }

    // Update user_game_history table
    let userHistory = await user_game_history.findOne({ where: { user_key: userId } });
    if (userHistory) {
      await userHistory.update({
        previousactivity,
        previousgameplayed,
      });
    } else {
      userHistory = await user_game_history.create({
        user_key: userId,
        previousactivity,
        previousgameplayed,
      });
    }

    res.redirect(`/users/${userId}/edit`);
  } catch (error) {
    console.error('Error updating user:', error);
    res.status(500).send('Internal Server Error: ' + error.message);
  }
};


app.post('/users/:id', updateUserHandler);
app.post('/users/:id/edit', updateUserHandler);

app.get("/users/:id/delete", async (req, res) => {
  try {
    const user = await user_game.findOne({ where: { id: req.params.id } });
    if (!user) {
      return res.status(404).send('User not found');
    }

    res.render('pages/delete', { user });
  } catch (error) {
    console.error('Error fetching user details:', error);
    res.status(500).send('Internal Server Error: ' + error.message);
  }
});

app.post('/users/:id/deletedata', async (req, res) => {
  try {
    const user = await user_game.findOne({
      where: { id: req.params.id },
      include: [
        { model: user_game_biodata, as: 'user_game_biodata' },
        { model: user_game_history, as: 'user_game_history' },
      ],
    });

    if (!user) {
      return res.status(404).send('User not found');
    }

    // Delete associated records first (user_game_biodata and user_game_history)
    if (user.user_game_biodata) {
      await user.user_game_biodata.destroy();
    }
    
    if (user.user_game_history) {
      await user.user_game_history.destroy();
    }
    
    // Delete the main user record
    await user.destroy();

    res.redirect('/users'); // Redirect to the user listing page after successful deletion
  } catch (error) {
    console.error('Error deleting user:', error);
    res.status(500).send('Internal Server Error: ' + error.message);

    // Additional logging to show the error on the terminal
    console.error(error);
  }
});


sequelize.sync({ force: false }).then(() => {
  console.log("Database synced successfully.");
}).catch((error) => {
  console.error("Unable to sync database:", error);
});

app.listen(3000, () => {
  console.log("Server started on port 3000");
});
