'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('user_game_history', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_key: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'user_game', // This references the 'user-game' table
          key: 'id', // This references the 'uuid' field in the 'user-game' table
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      previousactivity: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      previousgameplayed: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('user_game_history');
  }
};